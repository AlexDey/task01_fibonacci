/*
 * Copyright (c) Oleksandr Deineka
 * This software is created for educational purposes
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Class is a result of performing Task02_Basic
 *
 * @author Oleksandr Deineka deyneka.alexandr@gmail.com
 * @version 1.0 10 Nov 2018
 */
public class Solution {
    /**
     * oddsList is created for storing odd numbers
     */
    static ArrayList<Integer> oddsList;
    /**
     * evenList is created for storing even numbers
     */
    static ArrayList<Integer> evenList;
    /**
     * fibonacciList is created for storing Fibonacci numbers
     */
    static ArrayList<Integer> fibonacciList;
    /**
     * reader is created for reading input data from the console
     */
    static BufferedReader reader = new BufferedReader
            (new InputStreamReader(System.in));
    /**
     * a is a lower limit of the interval
     */
    static int a;
    /**
     * b is a higher limit of the interval
     */
    static int b;
    /**
     * N is a set of Fibonacci numbers
     */
    static int N;

    /**
     * start point of the program
     *
     * @param args command line values
     * @throws IOException throws an exception
     */
    public static void main(String[] args) throws IOException {
        startProg();
        viewMainMenu();
    }

    /**
     * method starts the program, runs initLists and initFibonacciList methods
     *
     * @throws IOException throws an exception
     */
    static void startProg() throws IOException {
        System.out.println("Please, enter the interval in order to go to " +
                "the next step");
        System.out.println("Enter the first integer:");
        a = Integer.parseInt(reader.readLine());
        System.out.println("Enter the second integer:");
        b = Integer.parseInt(reader.readLine());
        while (b <= a) {
            System.out.println("The second integer must be more than " +
                    a + ". " + "Please re-enter the second integer");
            b = Integer.parseInt(reader.readLine());
        }
        System.out.println("Please, enter the size of Fibonacci set: ");
        N = Integer.parseInt(reader.readLine());
        while (N <= 0) {
            System.out.println("The size of set must be more than 0. " +
                    "Re-enter it: ");
            N = Integer.parseInt(reader.readLine());
        }
        initLists(a, b);
        initFibonacciList(N);
    }

    /**
     * method views the user's menu and runs runSwitch method
     *
     * @throws IOException throws an exception
     */
    static void viewMainMenu() throws IOException {
        System.out.println("Type the option number and press Enter: \n" +
                "1 Print odd numbers from start to the end of interval \n" +
                "2 Print even numbers from end to the start of interval \n" +
                "3 Print the sum of odd numbers \n" +
                "4 Print the sum of even numbers \n" +
                "5 Build Fibonacci numbers \n" +
                "6 Print percentage of odd Fibonacci numbers \n" +
                "7 Print percentage of even Fibonacci numbers \n" +
                "0 Exit");
        runSwitch();
    }

    /**
     * method deals with user's input
     *
     * @throws IOException throws an exception
     */
    static void runSwitch() throws IOException {
        int temp = Integer.parseInt(reader.readLine());
        switch (temp) {
            case 1:
                System.out.println("Odd numbers of the interval: " +
                        oddsList + "\n");
                viewMainMenu();
                break;
            case 2:
                System.out.println("Even numbers of the interval: " +
                        evenList + "\n");
                viewMainMenu();
                break;
            case 3:
                System.out.println("The sum of odd numbers: " +
                        sum(oddsList) + "\n");
                viewMainMenu();
                break;
            case 4:
                System.out.println("The sum of even numbers: " +
                        sum(evenList) + "\n");
                viewMainMenu();
                break;
            case 5:
                System.out.println("Fibonacci numbers: " +
                        fibonacciList + "\n");
                viewMainMenu();
                break;
            case 6:
                System.out.println("Percentage of odd Fibonacci numbers is: " +
                        calculatePersentage(fibonacciList, "odd") + "\n");
                viewMainMenu();
                break;
            case 7:
                System.out.println("Percentage of even Fibonacci numbers is: " +
                        calculatePersentage(fibonacciList, "even") + "\n");
                viewMainMenu();
                break;
            case 0:
                break;
            default:
                System.out.println("You entered the wrong number");
                viewMainMenu();
                break;
        }
    }

    /**
     * method initializes oddsList and evenList
     *
     * @param a lower and lower limit of the interval
     * @param b lower and higher limit of the interval
     */
    static void initLists(int a, int b) {
        oddsList = new ArrayList<Integer>();
        evenList = new ArrayList<Integer>();
        for (int i = a; i <= b; i++) {
            if (i % 2 != 0) {
                oddsList.add(i);
            } else {
                evenList.add(i);
            }
        }
        Collections.reverse(evenList);
    }

    /**
     * method calculates the sum of numbers
     *
     * @param list ArrayList for calculating its data sum
     * @return int as a sum of numbers
     */
    static int sum(ArrayList<Integer> list) {
        int sum = 0;
        for (int i : list) {
            sum += i;
        }
        return sum;
    }

    /**
     * method initializes fibonacciList
     *
     * @param n - set of Fibonacci
     * @throws IOException throws an exception
     */
    static void initFibonacciList(int n) {
        fibonacciList = new ArrayList<Integer>();

        /* Declare an array to store Fibonacci numbers. */
        int f[] = new int[n + 2]; // 1 extra to handle case, n = 0
        int i;
        /* 0th and 1st number of the series are 0 and 1*/
        f[0] = oddsList.get(oddsList.size() - 1);
        f[1] = evenList.get(0);
        //String s = "Fibonacci numbers: " + f[0] + " " + f[1] + " ";
        fibonacciList.add(f[0]);
        fibonacciList.add(f[1]);
        for (i = 2; i <= n; i++) {/* Add the previous 2 numbers */
            f[i] = f[i - 1] + f[i - 2];
            fibonacciList.add(f[i]);
        }
    }

    /**
     * method calculates the percentage of odd or even numbers
     *
     * @param list - ArrayList for calculating operations
     * @param s    - string for switching between odds and evens
     * @return double as a percentage of numbers
     */
    static double calculatePersentage(ArrayList<Integer> list, String s) {
        int countOdds = 0;
        for (int i : list) {
            if (i % 2 != 0) {
                countOdds++;
            }
        }
        int countEvens = list.size() - countOdds;
        return s.equals("even") ? countEvens * 100.0 / list.size() :
                countOdds * 100.0 / list.size();
    }
}

